[TOC]

**解决方案介绍**
===============
基于华为云服务器极致性价比和对象存储服务低成本存储海量数据等优势，该方案可帮助您在华为云弹性云服务器上快速搭建SaaS云建站系统，您可以通过部署wangmarketCMS建站系统，创建上千个无技术门槛，100%自由度的网站，并根据开源代码进行定制化开发。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/build-a-cms-based-on-open-source-wangmarket.html

**架构图**
---------------
![方案架构](./document/build-a-CMS-based-on-open-source-wangmarket.png)

**架构描述**
---------------
该解决方案部署如下资源：

1.创建一台弹性云服务器ECS用于搭建SaaS云建站系统的环境。

2.创建一个弹性公网EIP绑定到弹性云服务器，提供访问公网和被公网访问能力。

3.创建一个对象存储服务OBS，用于存储网站静态页面及图片等。

4.创建一个云数据库RDS for MySQL，用于数据存储。

**组织结构**
---------------
``` lua
build-a-CMS-based-on-open-source-wangmarket
├── build-a-CMS-based-on-open-source-wangmarket-entry.tf.json -- 资源编排模板
├── build-a-CMS-based-on-open-source-wangmarket-enhanced.tf.json -- 资源编排模板
├── userdata
    ├── install_wangmarket_entry.sh -- 脚本配置文件
	├── install_wangmarket_enhanced.sh -- 脚本配置文件
```
**开始使用**
---------------
1、登录华为云弹性服务器控制台，您可以看到该方案创建出来的弹性云服务器ECS，并查看该云服务器绑定的弹性公网EIP。在任意浏览器输入弹性公网EIP，即可访问wangmarketCMS建站系统。点击开始安装系统。

图1 访问wangmarketCMS建站系统

![ 访问wangmarketCMS建站系统](./document/readme-image-001.png)

2、您可以选择将网站所需的静态资源如图片，html页面等存储在该服务器中，则此处点击“默认-服务器本身存储”。您也可以通过配置对象存储服务，将静态资源放置于OBS中。

图2 选择存储方式

![选择存储方式](./document/readme-image-002.png)

3、您需要为网站设置域名，如果您暂时没有域名，请参考部署指南进行域名注册及域名泛解析设置。

图3 设置域名

![设置域名](./document/readme-image-003.png)

4、如图所示即成功安装系统，您可以登录如下三种后台。网站管理后台将用于建立，维护，管理具体某个网站。代理后台将用于开通网站，开通下级代理，为某个网站延长使用时间，冻结、解冻某个网站。总管理后台将用于开发人员维护系统。

图4 成功安装系统

![成功安装系统](./document/readme-image-004.png)





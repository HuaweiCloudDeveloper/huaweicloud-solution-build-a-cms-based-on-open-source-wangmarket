#!/bin/bash
mkdir /readyFile
cd /readyFile || exit
# download jdk 
wget https://mirrors.huaweicloud.com/java/jdk/8u151-b12/jdk-8u151-linux-x64.rpm
rpm -ivh jdk-8u151-linux-x64.rpm
{
  echo 'export JAVA_HOME=/usr/java/jdk1.8.0_151/'
  echo 'export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar'
  echo 'export PATH=$PATH:$JAVA_HOME/bin'
} >> /etc/profile
export JAVA_HOME=/usr/java/jdk1.8.0_151/
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
# download Tomcat8
mkdir /readyFile/tomcat8
cd /readyFile/tomcat8 || exit
wget http://down.zvo.cn/centos/apache-tomcat-8.5.51.zip
yum -y install unzip
unzip apache-tomcat-8.5.51.zip
cd webapps || exit
rm -rf docs
rm -rf examples
rm -rf host-manager
rm -rf manager
cd ROOT || exit
rm -rf ./*
# move tomcat8 to /mnt 
cd /readyFile || exit
mv tomcat8/ /mnt/
# config Tomcat8
echo 'JAVA_HOME=/usr/java/jdk1.8.0_151/'>>/etc/rc.local
echo 'export JAVA_HOME'>>/etc/rc.local
# config startup
echo '/mnt/tomcat8/bin/startup.sh'>>/etc/rc.d/rc.local
# change mode
chmod +x /mnt/tomcat8/bin/startup.sh
chmod +x /etc/rc.d/rc.local
# download wangmarket package
cd /readyFile || exit
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/build-a-cms-based-on-open-source-wangmarket/open-source-software/wangmarket.zip
cp wangmarket.zip /mnt/tomcat8/webapps/ROOT/wangmarket.zip
cd /mnt/tomcat8/webapps/ROOT/ || exit
unzip wangmarket.zip
rm -rf wangmarket.zip
cd /mnt/tomcat8/bin/ || exit
#install mysql and create database
yum install mysql -y
mysql -h "$1" -P 3306 -u root -p"$2" -e "create database IF NOT EXISTS wangmarket"
# execute sql 
cd /root || exit
wget https://gitee.com/mail_osc/wangmarket/raw/master/else/wangmarket.sql
mysql -h "$1" -P 3306 -u root -p"$2" wangmarket -e "source /root/wangmarket.sql"
# config the application properties 
sed -i "s#^database.ip=.*#database.ip=$1#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i "s#^spring.datasource.password=.*#spring.datasource.password=$2#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '/spring.datasource.url/s/^#//'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '/spring.datasource.driver/s/^#//'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '29s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '30s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '31s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
# start tomcat
sh /mnt/tomcat8/bin/startup.sh
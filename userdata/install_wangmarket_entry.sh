#!/bin/bash
mkdir /readyFile
cd /readyFile || exit
# download jdk 
wget https://mirrors.huaweicloud.com/java/jdk/8u151-b12/jdk-8u151-linux-x64.rpm
rpm -ivh jdk-8u151-linux-x64.rpm
{
  echo 'export JAVA_HOME=/usr/java/jdk1.8.0_151/'
  echo 'export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar'
  echo 'export PATH=$PATH:$JAVA_HOME/bin'
} >> /etc/profile
export JAVA_HOME=/usr/java/jdk1.8.0_151/
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
# download Tomcat8
mkdir /readyFile/tomcat8
cd /readyFile/tomcat8 || exit
wget http://down.zvo.cn/centos/apache-tomcat-8.5.51.zip
yum -y install unzip
unzip apache-tomcat-8.5.51.zip
cd webapps || exit
rm -rf docs
rm -rf examples
rm -rf host-manager
rm -rf manager
cd ROOT || exit
rm -rf ./*
# move tomcat8 to /mnt 
cd /readyFile || exit
mv tomcat8/ /mnt/
# config Tomcat8
echo 'JAVA_HOME=/usr/java/jdk1.8.0_151/'>>/etc/rc.local
echo 'export JAVA_HOME'>>/etc/rc.local
# config startup
echo '/mnt/tomcat8/bin/startup.sh'>>/etc/rc.d/rc.local
# change mode
chmod +x /mnt/tomcat8/bin/startup.sh
chmod +x /etc/rc.d/rc.local
# download wangmarket package
cd /readyFile || exit
wget https://documentation-samples.obs.cn-north-4.myhuaweicloud.com/solution-as-code-publicbucket/solution-as-code-moudle/build-a-cms-based-on-open-source-wangmarket/open-source-software/wangmarket.zip
cp wangmarket.zip /mnt/tomcat8/webapps/ROOT/wangmarket.zip
cd /mnt/tomcat8/webapps/ROOT/ || exit
unzip wangmarket.zip
rm -rf wangmarket.zip
# install mysql
yum -y remove mariadb-libs.x86_64
rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
wget http://repo.mysql.com/mysql57-community-release-el7-8.noarch.rpm
yum -y install mysql57-community-release-el7-8.noarch.rpm
yum -y install gcc gcc-c++ gcc-g77 autoconf automake zlib* fiex* libxml* ncurses-devel libmcrypt* libtool-ltdl-devel* make cmake bison git openssl openssl-devel
yum -y install mysql-community-server
# start mysql
systemctl start mysqld
systemctl enable mysqld
systemctl daemon-reload
# start firewall
systemctl start firewalld
# add port 3306 and 80
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
# reload firewall
firewall-cmd --reload
# restart mysql
systemctl restart mysqld
# config mysql 
mysql_oldpwd_temp=`grep 'temporary password' /var/log/mysqld.log`
mysql_oldpwd=${mysql_oldpwd_temp#*root@localhost: }
db_name=wangmarket
mysql -uroot -p"$mysql_oldpwd" --connect-expired-password -e "set password=password('$1');"
# mysql create wangmarket user
mysql -uroot -p"$1" -e "create user '$2'@'%' IDENTIFIED BY '$3'"
# config the properties 
sed -i "s#^spring.datasource.username=.*#spring.datasource.username=$2#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i "s#^spring.datasource.password=.*#spring.datasource.password=$3#g" /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '/spring.datasource.url/s/^#//'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '/spring.datasource.driver/s/^#//'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '29s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '30s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
sed -i '31s/^/#&/g'  /mnt/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties
# create wangmarket database
mysql -uroot -p"$1" -e "create database IF NOT EXISTS ${db_name}"
# execute sql 
cd /root || exit
wget https://gitee.com/mail_osc/wangmarket/raw/master/else/wangmarket.sql
mysql -uroot -p"$1" "$db_name" -e "source /root/wangmarket.sql"
# grant privileges for user
mysql -uroot -p"$1" -e "grant all privileges on $db_name.* to $2@'%' identified by '$3'"
# start tomcat
sh /mnt/tomcat8/bin/startup.sh